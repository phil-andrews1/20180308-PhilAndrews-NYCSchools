import React, { Component } from 'react';
import { Provider } from 'react-redux'

import SchoolList from "./components/SchoolList";
import store from "./shared/Store";
import { fetchAllSchoolsAndScores } from "./shared/actions/fetch/fetchAllSchools";


export default class App extends Component {

  componentWillMount() {

    // As soon as the App mounts we'll send out two GET requests. I'm doing it this way because
    // we can't (see note 1 below) query for individual schools when one is selected from the list.
    // The API endpoint for scores only provides a full JSON table of all 440 schools

    store.dispatch({type: 'FETCH_SCHOOLS', payload: fetchAllSchoolsAndScores()})

    // I'm choosing to fetch everything right here because this app is useless without
    // both pieces of data. No one coming to this app is looking solely for a list of schools in NYC.
    // So we might as well fetch it all as once instead of breaking it apart.

  }


  render() {
    return (
      <Provider store={store} >
        <SchoolList />
      </Provider>
    )
  }
}

// note 1:
// If there was going to be a database attached regardless, I would gather all the data and put
// it into a queryable format within that database. So when a user wanted data for a single school
// the program would query the db for that one school. Pulling entire data sets is poor practice
// since it consumes data and resources that are unnecessary. Best practice is to only require
// as much data as is necessary. This is less true on desktop, but it's still good practice.
