export const colors = {
  white: 'white',
  black: '#263238',
  blackLight: '#494949',
  blackDark: '#000000',
  primary: '#2979ff',
  primaryLight: '#75a7ff',
  primaryDark: '#004ecb',
  secondary: '#B7FDFE',
  red: '#FF5C42',
  yellow: '#FFBA29',
  green: '#98CC0C'
}

export const gradient = [
  '#AB0D10',
  '#B82C0E',
  '#DD3900',
  '#D29A00',
  '#FFBA29',
  '#F1E613',
  '#98CC0C',
  '#6AC600',
  '#11BF00'
]

