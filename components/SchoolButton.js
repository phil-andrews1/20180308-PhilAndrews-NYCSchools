import React from 'react'
import { TouchableOpacity, View, Text, Dimensions } from 'react-native'

const _ = require('lodash')

import { gradient, colors } from './Color'
import store from "../shared/Store";
const { height, width } = Dimensions.get('window') 

class SchoolButton extends React.Component {

  onPress() {
    store.dispatch({type: 'SHOW_MODAL', school: this.props.school})
  }

  assignBgColor(percentile) {

    const { statsTotalScore } = this.props.school

    // We have to account for incomplete data from the database
    // If there are no scores set the indicator to neutral (white)
    if (_.isNull(statsTotalScore) || statsTotalScore == 1) {
      return colors.white
    }

    switch(percentile) {
      case 1:
        return gradient[gradient.length-9]
      case 5:
        return gradient[gradient.length-8]
      case 10:
        return gradient[gradient.length-7]
      case 25:
        return gradient[gradient.length-6]
      case 50:
        return gradient[gradient.length-5]
      case 75:
        return gradient[gradient.length-4]
      case 90:
        return gradient[gradient.length-3]
      case 95:
        return gradient[gradient.length-2]
      case 100:
        return gradient[gradient.length-1]
      default:
        return
    }
  }

  render() {

    const { school } = this.props

    const styles = {
      container: {
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start',
        height: 90,
        width: width,
        backgroundColor: colors.white,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingTop: 16,
        paddingBottom: 16,
      },
      innerContainer: {
        flex: 1,
        height: 90,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
      },
      rankIndicator: {
        height: 90,
        width: 10,
        backgroundColor: this.assignBgColor(school.statsTotalScorePercentile)
      },
      schoolName: {
        paddingLeft: 8,
        paddingRight: 8,
        width: width * 0.9,
        fontWeight: '600'
      }
    }

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.onPress()}
      >
        <View style={styles.innerContainer}>
          <View style={styles.rankIndicator}/>
          <Text
            style={styles.schoolName}
            numberOfLines={2}
          >{_.startCase(_.toLower(school.school_name))}</Text>
        </View>
      </TouchableOpacity>
    )
  }

}

export default SchoolButton
