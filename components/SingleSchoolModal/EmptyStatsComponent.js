import React from 'react'
import { connect } from 'react-redux'
import { View, Text, Dimensions } from 'react-native'

const _ = require('lodash')

import {colors } from '../Color'
const { height, width } = Dimensions.get('window')

class EmptyStatsComponent extends React.Component {

  render() {

    const styles = {
      container: {
        position: 'absolute',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        top: 130,
        height: height - 135,
        width: width
      },
      textStyle: {
        color: colors.black,
        fontSize: 24,
        fontWeight: '800',
        textAlign: 'center',
        width: width - 32,
      }
    }


    return (
      <View style={styles.container}>
        <Text style={styles.textStyle}>There is no SAT data for this school</Text>
      </View>
    )
  }

}

export default EmptyStatsComponent
