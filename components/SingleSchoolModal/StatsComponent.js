import React from 'react'
import { View, Text, Dimensions } from 'react-native'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'


const _ = require('lodash')

import { colors } from '../Color'
const { height, width } = Dimensions.get('window')

class StatsComponent extends React.Component {

  render() {

    const { school } = this.props

    const styles = {
      scoresContainer: {
        position: 'absolute',
        top: 130,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: height - 130,
        width: width,
      },
      scoreSection: {
        flex: 1,
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
        width: width,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 1,
      },
      subScore: {
        fontSize: 24,
        fontWeight: '700',
        color: colors.black
      },
      totalScore: {
        fontSize: 36,
        fontWeight: '900',
        color: colors.black
      }
    }

    return (
      <View style={styles.scoresContainer}>
        <View style={styles.scoreSection}>
          <MaterialCommunityIcon
            name="format-superscript"
            color={colors.primary}
            size={50}
          />
          <Text style={styles.subScore}>{school.statsMathScore}</Text>
        </View>
        <View style={styles.scoreSection}>
          <MaterialCommunityIcon
            name="book-open-page-variant"
            color={colors.primary}
            size={50}
          />
          <Text style={styles.subScore}>{school.statsReadingScore}</Text>
        </View>
        <View style={styles.scoreSection}>
          <MaterialCommunityIcon
            name="lead-pencil"
            color={colors.primary}
            size={50}
          />
          <Text style={styles.subScore}>{school.statsWritingScore}</Text>
        </View>
        <View style={styles.scoreSection}>
          <Text style={styles.totalScore}>{school.statsTotalScore}</Text>
        </View>
      </View>
    )
  }

}

export default StatsComponent
