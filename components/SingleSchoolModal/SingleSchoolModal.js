import React from 'react'
import { connect } from 'react-redux'
import { Modal, View, Text, Dimensions, TouchableOpacity, ScrollView } from 'react-native'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'

const _ = require('lodash')

import { colors } from '../Color'
import store from "../../shared/Store";
import StatsComponent from "./StatsComponent";
import EmptyStatsComponent from "./EmptyStatsComponent";
const { height, width } = Dimensions.get('window')

class SingleSchoolModal extends React.Component {


  render() {

    const { modal } = this.props
    const { school } = modal

    // This components renders on launch prior to being made active by the user,
    // Therefore we need to prevent rendering when no data is present

    if (_.isEmpty(school)) {
      return null
    }

    const styles = {
      container: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: height,
        width: width,
        backgroundColor: colors.black,
      },
      header: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
        width: width,
        height: 65,
        paddingLeft: 16,
        paddingRight: 16,
        backgroundColor: colors.black
      },
      closeButton: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 65,
        width: 65,
      },
      nameContainerStyle: {
        position: 'absolute',
        top: 60,
        left: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: width,
        height: 70,
        backgroundColor: colors.black,
      },
      schoolName: {
        color: colors.white,
        fontWeight: '800',
        padding: 8,
      },
    }

    return (
      <Modal
        visible={modal.showModal}
        style={styles.container}
        animationType="slide"
      >
        <View header={styles.header}>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => store.dispatch({type: 'CLOSE_MODAL'})}
          >
            <MaterialCommunityIcon
              name="chevron-down"
              color={colors.primary}
              size={50}
            />
          </TouchableOpacity>
        </View>
        {
          _.isNull(school.statsTotalScore) || _.isUndefined(school.statsTotalScore) ?
            <EmptyStatsComponent />
            : <StatsComponent school={school}/>
        }

        <View style={styles.nameContainerStyle}>
          <Text
            style={styles.schoolName}
            numberOfLines={3}
          >{school.school_name}</Text>
        </View>
      </Modal>
    )
  }

}

export default connect(store => ({
  modal: store.modal,
}))(SingleSchoolModal)
