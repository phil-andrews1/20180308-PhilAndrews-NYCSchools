import React from 'react'
import { connect } from 'react-redux'
import { View, Dimensions, FlatList, StatusBar, TouchableOpacity, Text, ActivityIndicator } from 'react-native'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'
const _ = require('lodash')

import SchoolButton from "./SchoolButton"
import { colors } from './Color'
import SingleSchoolModal from "./SingleSchoolModal/SingleSchoolModal";
import { fetchAllSchoolsAndScores } from "../shared/actions/fetch/fetchAllSchools";
const { height, width } = Dimensions.get('window')

class SchoolList extends React.Component {


  sortSchoolsAlphabetically() {
    this.props.dispatch({type: 'SORT_SCHOOLS_ALPHABETICALLY'})
  }

  sortSchoolsByScore() {
    this.props.dispatch({type: 'SORT_SCHOOLS_BY_SCORES'})
  }

  keyExtractor = (item) => item.id

  renderItem = (feedItem) => {
    return (
      <SchoolButton
        school={feedItem.item}
      />
    )
  }

  retryLoad() {
    this.props.dispatch({type: 'FETCH_SCHOOLS', payload: fetchAllSchoolsAndScores()})
  }

  render() {

    const { allSchools } = this.props

    const styles = {
      container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      },
      activityIndicator: {
        position: 'absolute',
        top: 200,
        display: allSchools.isFetching ? 'flex' : 'none',
        justifyContent: 'center',
        alignItems: 'center',
        width: width,
        height: height/2 - 60,
      },
      errorContainer: {
        display: allSchools.isFetchError && !allSchools.isFetching ? 'flex' : 'none',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 100,
        height: height/2,
        width: width,
      },
      error: {
        width: width - 32,
        color: colors.white,
        textAlign: 'center',
        fontSize: 24,
        marginBottom: 24,
      },
      headerContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        backgroundColor: colors.black,
        height: 60,
      },
      headerText: {
        color: colors.white,
        fontWeight: '800',
        fontSize: 16,
        marginTop: 16,
      },
      scrollViewShell: {
        height: height - 60,
        width: width,
        marginTop: 60,
        backgroundColor: colors.black
      },
      innerContent: {
        width: width,
        paddingBottom: 60,
      },
      sortButtonContainer: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: width,
        height: 60,
        backgroundColor: colors.black
      },
      sortButton: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
      },
      buttonTextStyle: {
        color: colors.primary,
        fontWeight: '800',
        fontSize: 18,
      }

    }

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="blue"
          barStyle="light-content"
        />
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>NYC High Schools</Text>
        </View>
        <FlatList
          data={allSchools.schools}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
          style={styles.scrollViewShell}
          contentContainerStyle={styles.innerContent}
        />
        <View style={styles.sortButtonContainer}>
          <TouchableOpacity
            style={styles.sortButton}
            onPress={() => this.sortSchoolsAlphabetically()}
          >
            <Text style={styles.buttonTextStyle}>Sort A-Z</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.sortButton}
            onPress={() => this.sortSchoolsByScore()}
          >
            <Text style={styles.buttonTextStyle}>Sort By Score</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.activityIndicator}>
          <ActivityIndicator
            size='large'
            color={colors.white}
            animating={allSchools.isFetching}
          />
        </View>
        <View style={styles.errorContainer}>
          <Text style={styles.error}>{allSchools.fetchError}</Text>
          <TouchableOpacity onPress={() => this.retryLoad()}>
            <MaterialCommunityIcon
              name="refresh"
              color={colors.primary}
              size={50}
            />
          </TouchableOpacity>
        </View>
        <SingleSchoolModal />
      </View>
    )
  }

}

export default connect(store => ({
  allSchools: store.allSchools,
}))(SchoolList)
