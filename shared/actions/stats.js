const _ = require('lodash')
const stats = require("stats-lite")

export function calculateStats(scores, schools) {

  // First we need to filter out the scores that do not match an entry in
  // the schools data
  const filteredScores = filterScoresForInclusionInSchoolsData(scores, schools)

  // Once filtered, we map all the scores to respective arrays for aggregate calculations
  const writing = _.without(_.map(filteredScores, 'sat_writing_avg_score'), 's')
  const reading = _.without(_.map(filteredScores, 'sat_critical_reading_avg_score'), 's')
  const math = _.without(_.map(filteredScores, 'sat_math_avg_score'), 's')
  const totalScores = _.map(filteredScores, (set) => {
    return parseInt(set.sat_writing_avg_score)
      + parseInt(set.sat_critical_reading_avg_score)
      + parseInt(set.sat_math_avg_score)
  })
  const scoreSets = []

  // Map from scores to each school entry so we can combine into one object
  // that can be passed around
  for (let i = 0; i < filteredScores.length; i++) {
    const score = filteredScores[i]
    const mathScore = parseInt(score.sat_math_avg_score)
    const writingScore = parseInt(score.sat_writing_avg_score)
    const readingScore = parseInt(score.sat_critical_reading_avg_score)

    // But since there are empty scores in scores we need to weed them out
    // This data set has empty values set to 's' ¯\_(ツ)_/¯
    let totalScore
    if (score.sat_writing_avg_score === 's') {
      score.sat_writing_avg_score === 's'
      totalScore = null
    } else {
      totalScore = mathScore + writingScore + readingScore
    }

    // Gather all score components and push for merge with the school data
    const stats = {
      dbn: score.dbn,
      statsTotalScore: totalScore,
      statsMathScore: mathScore,
      statsReadingScore: readingScore,
      statsWritingScore: writingScore,
      statsTotalScorePercentile: calculatePercentile(totalScores, totalScore),
      statsWritingScorePercentile: calculatePercentile(writing, writingScore),
      statsReadingScorePercentile: calculatePercentile(reading, readingScore),
      statsMathScorePercentile: calculatePercentile(math, mathScore),
    }
    scoreSets.push(stats)
  }

  return scoreSets
}

function calculatePercentile(scores, value) {
  try {
    const levels = [0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 0.9, 0.95]
    for (let i = 0; i < levels.length; i++) {
      const level = levels[i]
      const p = _.round(stats.percentile(scores, level))
      if (value <= p) {
        return level * 100
      } else if (value > p && level === 0.95) {
        return 100
      }
    }
  } catch(err) {
    return null
  }
}

function filterScoresForInclusionInSchoolsData(scores, schools) {
  const filteredScores = scores
  for (let i = 0; i < filteredScores.length; i++) {
    const score = filteredScores[i]

    // If schools does not include a scores associated DBN we don't want
    // to include it in aggregate calculations so we pull it from the set
    if (!_.some(schools, {dbn: score.dbn})) {
      _.pullAllBy(filteredScores, [{dbn: score.dbn}], 'dbn')
    }
  }
  return filteredScores
}

