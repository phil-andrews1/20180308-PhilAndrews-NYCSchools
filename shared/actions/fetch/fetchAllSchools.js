import { fetchGet } from "./request";
import { calculateStats } from "../stats";
const _ = require('lodash')

export async function fetchAllSchoolsAndScores() {
  // Fetch all the school metadata from the endpoint
  const schools = await fetchGet('https://data.cityofnewyork.us/resource/97mf-9njv.json')
    // Fetch all the SAT scores from the endpoint
    const scores = await fetchGet('https://data.cityofnewyork.us/resource/734v-jeq5.json')
      // Run them through a stats calculator to aggregate percentiles, averages, etc.
      const stats = calculateStats(scores, schools)
      // Then merge stats and metadata together to send to the store
      _.forEach(schools, (obj) => {
        _.merge(obj, _.find(scores, {dbn: obj.dbn}))
        _.merge(obj, _.find(stats, {dbn: obj.dbn}))
      })
      // On return from this component the SchoolList component will render
      return { schools: _.sortBy(schools, 'school_name'), scores: scores, stats: stats }
}
