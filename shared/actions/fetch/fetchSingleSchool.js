import { fetchGet } from "./request";

export async function fetchScores() {
  return await fetchGet('https://data.cityofnewyork.us/resource/734v-jeq5.json')
}
