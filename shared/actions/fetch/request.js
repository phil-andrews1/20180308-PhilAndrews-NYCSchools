
export async function fetchGet(url) {
  console.log(url)
  const request = await fetch(url)
  return await request.json()
}

export async function fetchPost(url, body) {
  console.log(url)
  const request = await fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': ' application/json',
    },
    body: JSON.stringify(body),
  })
  return await request.json()
}
