const _ = require('lodash')

const initialState = {
  showModal: false,
  school: {}
}

const SingleSchoolModalReducer = (state = initialState, action) => {

  let newState

  switch (action.type) {
    
    case 'SHOW_MODAL':
      newState = {
        ...state,
        showModal: true,
        school: action.school
      }
      break
    case 'CLOSE_MODAL':
      newState = {
        ...state,
        showModal: false,
      }
      break
    default:
      return state 
  }

  return newState
}

export default SingleSchoolModalReducer
