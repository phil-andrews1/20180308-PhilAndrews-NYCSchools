const _ = require('lodash')

const initialState = {
  isFetching: true,
  isFetchError: false,
  fetchError: null,
  schools: [],
  currentSortOrder: 'alphabetically'
}

const AllSchoolsReducer = (state = initialState, action) => {

  let newState

  switch (action.type) {

    case 'FETCH_SCHOOLS':
      newState = {
        ...state,
        isFetching: true
      }
      break
    case 'FETCH_SCHOOLS_FULFILLED':
      newState = {
        ...state,
        isFetching: false,
        isFetchError: false,
        schools: action.payload.schools,
        scores: action.payload.scores
      }
      break
    case 'FETCH_SCHOOLS_REJECTED':
      newState = {
        ...state,
        isFetching: false,
        isFetchError: true,
        fetchError: action.payload.message
      }
      break
    case 'SORT_SCHOOLS_ALPHABETICALLY':
      newState = {
        ...state,
        schools: _.sortBy(state.schools, 'school_name'),
        currentSortOrder: 'alphabetically'
      }
      break
    case 'SORT_SCHOOLS_BY_SCORES':

      // Order by total score button goes both ascending and descending
      // We set it according to the current sort state
      let orderType = 'desc'
      if (state.currentSortOrder === 'desc') {
        orderType = 'asc'
      }
      newState = {
        ...state,

        schools: _.orderBy(state.schools, [( o ) => { return o.statsTotalScore || 0 }], [orderType]),
        currentSortOrder: orderType
      }
      break
    default:
      return state
  }

  return newState
}

export default AllSchoolsReducer
