import { combineReducers } from 'redux'

import AllSchoolsReducer from './AllSchoolsReducer'
import SingleSchoolModalReducer from "./SingleSchoolModalReducer";

export default combineReducers({
  allSchools: AllSchoolsReducer,
  modal: SingleSchoolModalReducer
})
