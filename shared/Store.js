import { applyMiddleware, createStore } from 'redux'
import promise from 'redux-promise-middleware'
import combinedReducers from './reducers/CombinedReducers'

const middleware = applyMiddleware(
  promise(),
)

const store = createStore(combinedReducers, middleware)

export default store
